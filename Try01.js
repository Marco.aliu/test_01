$( document ).ready(function() {
// COLLAPSE NAVBAR //

  $("#toggle").click(function(){
      $("#ul-hook").toggleClass("active");
  });

// LOGIN & REGISTER //

  $("form").hide();
  $("#register-box").hide();
  
  $("#login-btn").click(function(){
      $("#main-btn").hide();
      $("#register-box").hide();
      $("form").show();
      $("#login-box").show();
  });

  $("#sing-btn").click(function(){
      $("#main-btn").hide();
      $("#login-box").hide();
      $("form").show();
      $("#register-box").show();
  });
  
  $(".cancel-btn").click(function(){
      $("form").hide();
      $("#main-btn").show();
  });

// CONTAINERS ONCLICK //

  $(".p-text").hide();

  $( "#ctn-01" ).click(function() {
    $("#H-ID-01").toggle();
    $("#P-01").toggle();
  });

  $( "#ctn-02" ).click(function() {
    $("#H-ID-02").toggle();
    $("#P-02").toggle();
  });

  $( "#ctn-03" ).click(function() {
    $("#H-ID-03").toggle();
    $("#P-03").toggle();
  });
  
});